=======
Gallery
=======

Band-structure
==============

:ref:`scissors band structure`

.. image:: ../documentation/scissors/mos2ws2.png


Electron localisation function (ELF)
====================================

Water molecule (ELF=0.8):

.. raw:: html
	:file: h2o-elf.html

:mod:`gpaw.elf`, :download:`h2o-elf.py`.


Wave-function isosurfaces for water
===================================

HOMO-3, HOMO-2, HOMO-1 and HOMO:

.. list-table::

   * - .. image:: homo-3.png
     - .. image:: homo-2.png
     - .. image:: homo-1.png
     - .. image:: homo-0.png

LUMO and LUMO+1:

===================== =====================
.. image:: lumo+0.png .. image:: lumo+1.png
===================== =====================

:download:`h2o-iso.py`.
